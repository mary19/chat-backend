/**
 * Created by maryna on 30.06.17.
 */
'use strict';
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);

let users = [];
let messages = [];
io.on('connection', (socket) => {
    socket.on('add-message', (message) => {
        messages.unshift(message);
        io.emit('get-messages', messages);
    });
    socket.on('user-enter', (user) => {
        if (!users.some(person => person.name === user.name)) {
            users.push(user);
            io.emit('get-users', users);
        }
    });
    socket.on('get-users', () => {
        io.emit('get-users', users);
    });
    socket.on('get-messages', (from, to) => {
        let data = messages.filter(message => (message.from === from && message.to === to) || (message.from === to && message.to === from));
        io.emit('get-messages', data);
    });
});
http.listen(3000, () => {
    console.log('started on port 3000');
});